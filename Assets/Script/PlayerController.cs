using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb;
    private enum State {idle,run,jump,falling };
    private State state = State.idle;
    public Animator anim;
    private Collider2D coll;
    [SerializeField] private LayerMask Ground;
    [SerializeField] private Text point,nyawa;
    public int item=0;
    public int healt;
    // Start is called before the first frame update
    void Start()
    {
        nyawa.text = healt.ToString();
        coll = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float hor = Input.GetAxis("Horizontal");
        if (hor>0)
        {
            rb.velocity = new Vector2(3, 0);
            transform.localScale = new Vector2(1, 1);
        }
        else if (hor<0)
        {
            rb.velocity = new Vector2(-3, 0);
            transform.localScale = new Vector2(-1, 1);
        }
        else
        {
        }
        if (Input.GetKey(KeyCode.W)&& coll.IsTouchingLayers(Ground))
        {
            rb.velocity = new Vector2(rb.velocity.x, 10f);
            state = State.jump;
        }
        VelocityState();
        anim.SetInteger("State", (int)state);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Item")
        {
            Destroy(collision.gameObject);
            item += 1;
            point.text = item.ToString()+" Buah";
        }
        if (collision.tag == "Saw")
        {
            if (healt > 0)
            {
                healt -= 1;
                nyawa.text = healt.ToString();

            }
            else
            {
                healt = 0;
                nyawa.text = healt.ToString();
            }

        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
    }
    void VelocityState()
    {
        if (state == State.jump)
        {
            if (rb.velocity.y < .1f)
            {
                state = State.falling;
            }
        }
        else if (state == State.falling)
        {
            if (coll.IsTouchingLayers(Ground))
            {
                state = State.idle;
            }
        }
        else if (Mathf.Abs(rb.velocity.x) > 2f)
        {
            state = State.run;
        }
        else
        {
            state = State.idle;
        }
    }
}
